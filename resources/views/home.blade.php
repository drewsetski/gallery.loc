@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                @foreach($users as $user)
                    <a href="{!! route('user.gallery', ['id' => $user->id]) !!}" class="list-group-item">
                        <span class="badge">{!! $user->photos_count !!}</span>
                        {!! $user->name !!}
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection