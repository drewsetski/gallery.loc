<footer>
    © {{ Carbon\Carbon::now()->year }} <a href="{{ url('/') }}">{{ env('PROJECT_NAME') }}</a>
</footer>