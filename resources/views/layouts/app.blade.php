<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <title>{!! isset( $title ) ? $title : env('PROJECT_NAME') !!}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/jquery.navobile.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/check.css" rel="stylesheet">
    <link href="/css/switchery.min.css" rel="stylesheet"/>
    <link href="/css/chosen.css" rel="stylesheet"/>
    <link href="/css/jquery.jgrowl.min.css" rel="stylesheet"/>
    <link href="/css/bootstrap-table.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">

    @yield('styles')

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="wrapper" id="content">
    <header class="header">
        <nav class="header-nav">
            <ul class="f-left">
                <li class="f-left">
                    <a href="#" id="show-sidebar" class="animated slideInLeftSmall"><i class="fa fa-bars fa-2x"></i></a>
                </li>
            </ul>
            <ul class="mainnav f-right">
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span>{!! Auth::user()->getAttribute('name') !!}</span>
                            <i class="fa fa-caret-down caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ route('user.gallery', ['id' => Auth::id() ]) }}"><i class="fa fa-user fa-2x" aria-hidden="true"></i>Profile</a>
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}"><i class="fa fa-sign-out fa-2x" aria-hidden="true"></i>Logout</a>
                            </li>
                        </ul>
                    </li>
                @else
                    <li class="square">
                        <a href="{!! url('/login') !!}"><i class="fa fa-sign-in fa-2x" aria-hidden="true"></i></a>
                    </li>
                    <li class="square">
                        <a href="{!! url('/register') !!}"><i class="fa fa-user-plus fa-2x" aria-hidden="true"></i></a>
                    </li>
                @endif
            </ul>
        </nav>
    </header><!-- .header-->

    <nav class="left-sidebar">
        <div class="sidebar-container">
            <div class="logo"><a href="/">{!! env('PROJECT_NAME') !!}</a></div>
            <ul>
                <li {{ Request::is('/' || 'home') ? ' class=active' : null }}><a href="/"><i class="fa fa-home fa-2x"></i><span>Home</span></a></li>
            </ul>
        </div>

        @include('partials._footer')
    </nav>

    <main>
        <div class="page-header">
            <h1>{!! isset( $title ) ? $title : env('PROJECT_NAME') !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')

                    @yield('content')
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->


</div><!-- end of wrapper -->

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-filestyle.min.js"></script>
<script src="/js/bootbox.js"></script>
<script src="/js/jquery.navobile.min.js"></script>
<script src="/js/switchery.min.js"></script>
<script src="/js/chosen.jquery.min.js"></script>
<script src="/js/jquery.jgrowl.min.js"></script>
<script src="/js/bootstrap-table.min.js"></script>
<script src="/js/app.js"></script>
<script src="/js/main.js"></script>

@yield('scripts')

</body>
</html>