<div class="col-lg-3 col-sm-6 mb20 photo">
    <div class="thumbnail" id="photo-{{ $photo->id }}">
        <div class="thumb">
            <img class="photo-thumb-image" src="{{ url('/uploads/images/thumbs/' . $photo->image) }}" alt="{{ $photo->name }}">
            <div class="caption-overflow">
                <span>
                    <a href="{{ url('/uploads/images/big/' . $photo->image) }}" data-lightbox="lightbox" class="btn photo-big-image"><i class="fa fa-2x fa-search-plus"></i></a>
                </span>
            </div>
        </div>
        <div class="caption">
            <h4 class="no-margin-top text-bold">
                <span class="photo-name">{{ $photo->name }}</span>
                @if($user->isOwner())
                    <div class="pull-right">
                        <a href="#" data-id="{{ $photo->id }}" class="text-muted openEditModal"><i class="fa fa-pencil-square fa-2x"></i></a>
                        <a href="#" data-id="{{ $photo->id }}" class="text-muted deletePhoto"><i class="fa fa-trash-o fa-2x"></i></a>
                    </div>
                @endif
            </h4>
            <div class="photo-description">{!! $photo->description !!}</div>
        </div>
    </div>
</div>