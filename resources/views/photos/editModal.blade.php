<div class="modal fade" id="editPhotoModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalLabel">Modal title</h4>
            </div>
            {!! Form::open(array( 'role' => 'form', 'files' => true, 'id' => 'formPhoto') ) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('image') !!}
                    {!! Form::file('image', array('class' => 'filestyle', 'data-buttonName' => 'btn-brand', 'data-icon' => 'true')) !!}
                </div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
                <div class="form-group thumbnail none">
                    <img src="" alt="" id="photoImage">
                </div>
                <div class="form-group">
                    {!! Form::label('name') !!}
                    {!! Form::text('name', null, array('class'=>'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description') !!}
                    {!! Form::textarea('description', null, array('class'=>'form-control', 'rows' => 5)) !!}
                </div>
                {!! Form::hidden('id') !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-method="" id="savePhoto">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>