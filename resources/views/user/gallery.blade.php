@extends('layouts.app')

@section('content')

    @if($user->isOwner())
        <button type="button" class="btn btn-brand btn-flat openCreateModal"><i class="fa fa-plus fa-2x"></i></button>
    @endif
    <div class="row">
        <div class="col-md-12 gallery">
            <div class="alert alert-info alert-with-icon gallery-empty none">
                <i class="alert-icon fa alert-icon-info"></i>
                {!! trans('message.user.no_photos') !!}
            </div>
            @if(count($photos))
                @foreach($photos as $photo)
                    @include('photos._photo')
                @endforeach
            @endif
        </div>
    </div>

    @include('photos.editModal')

@endsection

@section('styles')
    <link href="/css/lightbox.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="/js/lightbox.js"></script>
    <script>
        $(function(){
            if($('.gallery .photo').length == 0)
                $('.gallery-empty').removeClass('none');
        });
        $.ajaxSetup(
        {
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            }
        });
        var $showPath = '{!! route('photos.show', ['id' => null]) !!}';
        var $createPath = '{!! route('photos.create') !!}';
        var $updatePath = '{!! route('photos.update') !!}';
        var $deletePath = '{!! route('photos.delete', ['id' => null]) !!}';
        var $thumbImageFolder = '{{ url('/uploads/images/thumbs/') }}';
        var $bigImageFolder = '{{ url('/uploads/images/big/') }}';
        var $userName = '{!! $user->name !!}';

        $('#editPhotoModal').on('hidden.bs.modal', function (e) {
            document.title = $userName;
        })
        $('body').on('click', '.openCreateModal', function(event) {
            event.preventDefault();
            clearFormData();
            clearFormErrors();
            $('#editPhotoModal').modal();
            $('#modalLabel').html('Photo creating');
            document.title = 'Photo creating';
            $('#savePhoto').data('method', 'create');
        });
        $('body').on('click', '.openEditModal', function(event) {
            event.preventDefault();
            var photo_id = $(this).data('id');
            clearFormData();
            clearFormErrors();
            $.getJSON($showPath + '/' + photo_id, function (data) {
                loadDataToModal(data);
            });
            $('#editPhotoModal').modal();
            $('#photoImage').parent().removeClass('none');
            $('#savePhoto').data('method', 'update');
        });
        $('body').on('click', '.deletePhoto', function(event) {
            event.preventDefault();
            var photo_name = $(this).parent().prev().html();
            var photo_id = $(this).data('id');
            bootbox.confirm("You want to delete photo \"<strong>" + photo_name + "\"</strong>. Are you sure?", function(result) {
                if (result) {
                    $.ajax({
                        type: 'post',
                        url: $deletePath + '/' + photo_id,
                        dataType: 'json',
                        data: {id: photo_id},
                        cache: false,
                        processData: false,
                        contentType: false,
                        complete: function (data) {
                            switch (data.status) {
                                case 200:
                                    $('#photo-' + photo_id).parent().remove();
                                    $.jGrowl("{{ trans('message.photo.deleted') }}", {
                                        sticky: false,
                                        theme: "success",
                                        header: "Deleted"
                                    });
                                    if ($('.gallery .photo').length == 0)
                                        $('.gallery-empty').removeClass('none');
                                    break;
                                default:
                                    console.log(data)
                            }
                        },
                    })
                }
            });
        });
        $('body').on('click', '#savePhoto', function() {
            var type = $(this).data('method');
            var path;
            var photoData = getDataFromModal();
            var photo_id = $('#editPhotoModal input[name="id"]').val();
            switch (type) {
                case 'create':
                        path = $createPath;
                    break;
                case 'update':
                        path = $updatePath;
                    break;
            }
            $.ajax({
                type: 'post',
                url: path,
                data: photoData,
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false,
                xhr: function() {
                    $("#editPhotoModal").find(".progress-bar").width(0 + "%");
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) {
                        xhr.upload.addEventListener('progress', function (evt) {
                            var percent = (evt.loaded / evt.total) * 100;
                            $("#editPhotoModal").find(".progress-bar").width(percent + "%");
                        }, false);
                    }
                    return xhr;
                },
                complete: function(data) {
                    switch (data.status) {
                        case 200:
                            switch (type) {
                                case 'create':
                                    $('.gallery').append(data.responseText);
                                    $.jGrowl( "{{ trans('message.photo.created') }}", {
                                        sticky: false,
                                        theme:  "success",
                                        header: "Created"
                                    });
                                    $('.gallery-empty').addClass('none');
                                    break;
                                case 'update':
                                    $('#photo-' + photo_id).parent('div').replaceWith(data.responseText);
                                    $.jGrowl( "{{ trans('message.photo.updated') }}", {
                                        sticky: false,
                                        theme:  "success",
                                        header: "Updated"
                                    });
                                    break;
                            }
                            $('#editPhotoModal').modal('hide');
                            break;
                        default:
                            var errors = data.responseJSON;
                            clearFormErrors();
                            $.each(errors, function(key, value) {
                                $('#' + key).parent('.form-group').addClass('has-error');
                                $.each(value, function(k, val) {
                                    $('#' + key).parent('.form-group').append(
                                            '<span class="help-block">' +
                                            '<strong>' + val + '</strong>' +
                                            '</span>'
                                    );
                                });

                            });
                    }
                }
            });
        });
        function loadDataToModal(data) {
            $('#photoImage').attr('src', $thumbImageFolder + '/' + data['image']);
            $('#name').val(data['name']);
            $('.bootstrap-filestyle input[type="text"]').val(data['image']);
            $('#description').val(data['description']);
            $('input[name="id"]').val(data['id']);
            $('#modalLabel').html('Editing ' + data['name']);
            document.title = 'Editing ' + data['name'];
        }
        function clearFormData(){
            $('#photoImage').attr('src', '');
            $('.bootstrap-filestyle input[type="text"]').val('');
            $('#photoImage').parent().addClass('none');
            $('#image').val('');
            $('#name').val('');
            $('#description').val('');
            $('input[name="id"]').val('');
            $("#editPhotoModal").find(".progress-bar").width("0%");
        }
        function clearFormErrors(){
            $('.form-group').removeClass('has-error');
            $('.form-group .help-block').remove();
        }

        function getDataFromModal() {
            var data = new FormData($('#formPhoto')[0]);
            return data;
        }

        function loadDataToGallery(photo, data) {
            $(photo + ' img.photo-thumb-image').attr('src', $thumbImageFolder + '/' + data['image']);
            $(photo + ' img.photo-thumb-image').attr('alt', data['name']);
            $(photo + ' a.photo-big-image').attr('href', $bigImageFolder + '/' + data['image']);
            $(photo + ' span.photo-name').html(data['name']);
            $(photo + ' a.openEditModal').data('id', data['id']);
            $(photo + ' a.deletePhoto').data('id', data['id']);
            $(photo + ' div.photo-description').html(data['description']);
        }
    </script>
@endsection