<?php

return [

    'user' => [
        'not_found'         =>      'User has not been found',
        'no_photos'         =>      'User has no photos',
        'permission_update' =>      'You have no permission to update this photo',
        'permission_delete' =>      'You have no permission to delete this photo',
    ],

    'photo' => [
        'created'           =>      'Photo has been successfully created',
        'updated'           =>      'Photo has been successfully updated',
        'deleted'           =>      'Photo has been successfully deleted',
    ],

];
