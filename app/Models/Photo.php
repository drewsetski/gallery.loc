<?php

namespace App\Models;

use App\User;
use File;
use Illuminate\Database\Eloquent\Model;
use Image;

class Photo extends Model
{
    protected $table = 'photos';

    const IMAGE_THUMB_WIDTH = 320;
    const IMAGE_THUMB_HEIGHT = 240;
    const IMAGE_BIG_SIZE = 1200;

    const IMAGE_EXTENSION = 'jpeg';

    const IMAGE_MAX_SIZE = 5000;

    const PHOTO_NAME_MAX_LENGTH = 500;

    const DIR = 'uploads/images/';
    const DIR_THUMBS = Photo::DIR . 'thumbs/';
    const DIR_BIG = Photo::DIR . 'big/';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'description',
    ];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * Photo belongs to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Upload image
     *
     * @param $image
     * @param integer $id
     * @param bool $update
     */
    public static function saveImage($image, $id, $update = false)
    {
        $photo = self::find($id);
        Photo::checkDirectories();
        if ($update) {
            $old_filename = $photo->image;
            File::delete(Photo::DIR . $old_filename);
            File::delete(Photo::DIR_THUMBS . $old_filename);
            File::delete(Photo::DIR_BIG . $old_filename);
        }

        $filename = uniqid() . '.' . $image->getClientOriginalExtension();

        $img = Image::make($image->getRealPath());
        $img->save(public_path(Photo::DIR . $filename));
        if ($img->width() >= $img->height())
            $img->widen(Photo::IMAGE_BIG_SIZE)->save(public_path(Photo::DIR_BIG . $filename));
        else
            $img->heighten(Photo::IMAGE_BIG_SIZE)->save(public_path(Photo::DIR_BIG . $filename));

        $img->fit(Photo::IMAGE_THUMB_WIDTH, Photo::IMAGE_THUMB_HEIGHT)->save(public_path(Photo::DIR_THUMBS . $filename));

        $photo->update(['image' => $filename]);

        return $photo;
    }

    /**
     * Destroy photo
     *
     * @param array|int $id
     * @return int|void
     */
    public static function destroy($id)
    {
        $photo = self::find($id);
        $filename = $photo->image;
        File::delete(Photo::DIR . $filename);
        File::delete(Photo::DIR_THUMBS . $filename);
        File::delete(Photo::DIR_BIG . $filename);

        parent::destroy($id);
    }

    /**
     * Return validation rules
     *
     * @param $update
     * @return array
     */
    public static function getRules($update)
    {
        if(!$update) {
            return [
                'name'          => 'required|max:' . Photo::PHOTO_NAME_MAX_LENGTH,
                'description'   => '',
                'image'         => 'required|max:' . Photo::IMAGE_MAX_SIZE . '|mimes:' . Photo::IMAGE_EXTENSION,
            ];
        } else {
            return [
                'name'          => 'required|max:' . Photo::PHOTO_NAME_MAX_LENGTH,
                'description'   => '',
                'image'         => 'max:' . Photo::IMAGE_MAX_SIZE . '|mimes:' . Photo::IMAGE_EXTENSION,
            ];
        }
    }

    /**
     * Check if directories for uploads are created
     * Create if necessary
     */
    public static function checkDirectories()
    {
        if( ! File::isDirectory(Photo::DIR) ) {
            File::makeDirectory(Photo::DIR, 493, true);
        }
        if( ! File::isDirectory(Photo::DIR_THUMBS) ){
            File::makeDirectory(Photo::DIR_THUMBS, 493, true);
        }
        if( ! File::isDirectory(Photo::DIR_BIG) ){
            File::makeDirectory(Photo::DIR_BIG, 493, true);
        }

    }

}
