<?php

Route::pattern('id', '[0-9]+');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/', [
    'uses'  =>  'HomeController@index',
    'as'    =>  'home'
]);

Route::get('/{id}', [
    'uses'  =>  'UsersController@show',
    'as'    =>  'user.gallery'
]);

Route::group(['prefix' => 'photos', 'middleware' => 'auth'], function() {

    Route::get('/show/{id}', [
        'uses'  =>  'PhotosController@show',
        'as'    =>  'photos.show'
    ]);
    Route::post('/create', [
        'uses'  =>  'PhotosController@store',
        'as'    =>  'photos.create'
    ]);
    Route::post('/update', [
        'uses'  =>  'PhotosController@update',
        'as'    =>  'photos.update'
    ]);
    Route::post('/delete/{id}', [
        'uses'  =>  'PhotosController@destroy',
        'as'    =>  'photos.delete'
    ]);
});
