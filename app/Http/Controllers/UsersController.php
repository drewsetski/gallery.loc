<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Flash;

class UsersController extends Controller
{
    /**
     * Show user's photos by ID
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
            $title = $user->name;

            $photos = $user->photos;

            return view('user.gallery', [
                'title' => $title,
                'user' => $user,
                'photos' => $photos
            ]);
        } catch (ModelNotFoundException $ex) {
            Flash::error(trans('message.user.not_found'));
            return redirect()->route('home');
        }
    }
}
