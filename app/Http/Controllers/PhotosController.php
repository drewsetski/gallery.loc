<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;

class PhotosController extends Controller
{


    /**
     * Return photo model
     *
     * @param $id
     */
    public function show($id)
    {
        $photo = Photo::findOrFail($id)->toJson();

        return $photo;
    }

    /**
     * Store newly created photo in database
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $update = false;
        $rules = Photo::getRules($update);

        $validator = Validator::make($request->only(array_keys($rules)), $rules);
        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $photo = new Photo([
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);
        $user->photos()->save($photo);

        if ($image = $request->file('image')) {
            $photo = Photo::saveImage($image, $photo->getAttribute('id'), $update);
        }

        return response(view('photos._photo', [
            'photo' => $photo,
            'user' => $user
        ]), 200);
    }

    /**
     * Store newly created photo in database
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $update = true;
        $rules = Photo::getRules($update);

        $validator = Validator::make($request->only(array_keys($rules)), $rules);
        if ($validator->fails()) {
            return response($validator->messages(), 422);
        }

        $photo = Photo::findOrFail($request->input('id'));
        if ($photo->user->isOwner()) {
            $photo->update([
                'name' => $request->input('name'),
                'description' => $request->input('description')
            ]);

            if ($image = $request->file('image')) {
                $photo = Photo::saveImage($image, $photo->getAttribute('id'), $update);
            }

            return response(view('photos._photo', [
                'photo' => $photo,
                'user' => $user
            ]), 200);
        } else {
            return response(trans('message.user.permission_update'), 403);
        }
    }

    /**
     * Destroy photo and remove all its images
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        if (Photo::findOrFail($id)->user->isOwner()) {
            try {
                Photo::destroy($id);
                return 'deleted';
            } catch (\Exception $ex) {
                return $ex->getMessage();
            }
        } else {
            return response(trans('message.user.permission_delete'), 403);
        }
    }
}
