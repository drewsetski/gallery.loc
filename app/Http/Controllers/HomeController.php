<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show users with number of photos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Users';
        $users = User::withCount('photos')->get();

        return view('home', compact('title', 'users'));
    }
}
