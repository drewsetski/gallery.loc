$(function(){
	$(".left-sidebar").navobile({
	  cta: "#show-sidebar",
	  changeDOM: true,
	});
	$('[data-toggle="tooltip"]').tooltip();
	$('#content').click(function() {
		if($("#navobile-undefined").hasClass('navobile-navigation-visible'))
			$( "#show-sidebar" ).trigger( "click" );
	});
	$('#show-sidebar').click(function(){
		if($("#navobile-undefined").hasClass('navobile-navigation-visible')){
			$('#show-sidebar').removeClass('animated slideInLeftSmall');
			$('#show-sidebar i').removeClass('fa-bars');	
			$('#show-sidebar i').addClass('fa-times');	
			$('#show-sidebar').addClass('active');	
			$('#show-sidebar').addClass('animated rotateIn');

		}
		else{

			$('#show-sidebar').addClass('animated slideInLeftSmall');
			$('#show-sidebar i').addClass('fa-bars');	
			$('#show-sidebar i').removeClass('fa-times');	
			$('#show-sidebar').removeClass('active');
			$('#show-sidebar').removeClass('animated rotateIn');

		}
	});
	$('a.has-dropdown').click(function(e){
		e.preventDefault();
		$(this).next('ul.hidden-dropdown').slideToggle();
		if($(this).hasClass('dropped')){
			$(this).removeClass('dropped');
		}
		else{
			$(this).addClass('dropped');
		}
	});
	$('.left-sidebar ul.hidden-dropdown li.active').parent().prev().addClass('dropped');
});